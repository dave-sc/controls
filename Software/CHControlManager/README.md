# CH Control Manager Guide
CH Products developed the proprietary CH Control Manager software for use with their devices. The software is easy to use for basic settings and supports scripting for much more complex and advanced mappings. There are known issues with the software in Windows 10 that have suitable workarounds but can be very annoying.

#### Pros
* Easy to use, requires only one application
* Supports 11 response curves, inversion, deadzone, and remapping to key or mouse
* Save mappings and settings to a file that can be loaded and shared
* Supports advanced scripting with CMS

#### Cons
* Works with devices from CH Products only
* Joystick Gremlin has much better response curve and deadzone support
* No support for mapping two devices to a single binding which makes HOTAS + HOSAS impossible
* Using CMS for scripting is too complicated for most users

#### Known Issues
* Switching modes or updating a mapping requires restarting Star Citizen
* Sometimes the devices begin disappearing every time you click "Download" to apply a mapping

## Why we don't recommend CH Control Manager
1. It only supports CH devices and eventually you'll want to use a Thrustmaster, VIRPIL, VKB or even just a PS4 controller.
2. It doesn't support a HOTAS + HOSAS configuration because it cannot map your stick and throttle to the same bindings.
3. Joystick Gremlin has much better support for response curve, deadzone and allows for mapping pedals to two sides of the same axis.

TODO: link to the recommended Joystick Gremlin guide 

## Why you might use it anyway
1. You're only using CH devices.
2. You're happy with the 11 response curves available.
3. You don't care about combining HOTAS and HOSAS or mapping multiple devices to a single binding.
4. You don't care about mapping the pedals to a single axis.

TODO: link to what mapping the pedals to a single axis looks like

## Supported Devices

Check [the CH Products guide](../../Devices/CHProducts/README.md) for more information on compatible devices and printable bindings templates.

## Installation
Download the control manager from the [CH website](http://www.chproducts.com/13-29620-Downloads-and-Community-Links.php.html) where it may be listed as "optional control manager programming software". Execute the installer and follow the prompts.

## Modes
![Picture of Mode Buttons](images/CHControlManager_Direct_Mapped_Buttons.gif)

The control manager software operates in three modes:

* Off Mode: devices are "off" and do not show up in Windows
* Direct Mode: limited options and devices show up as DirectX devices in Windows like `CH Fighterstick USB`
* Mapped Mode: more advanced options and devices show up as `CH Control Manager Device 1`

![Animation of changing device names](images/CHControlManager_Direct_Mapped_Example.gif)

## Axis Settings
In Direct Mode you will need to click the "Test/Calibrate" button, choose a device, then click on the "Axis Settings" tab to change these values for an axis. 

![Picture of Axis Settings for Direct Mode](images/CH_Manager_calibrate_axis_settings.png)

In Mapped Mode you may update and save the settings as normal.

![Pictgure of Axis Settings for Mapped Mode](images/CH_Manager_map_axis_settings.png)

### Center
![Pictgure of Center Checkbox](images/CH_Manager_center_checkbox.png)

When this box is checked the axis is treated as having a central deadzone and can have a response curve assigned. This box will be set to automatically match what the devices actually provides. Normally this does not need to be changed.

In the case of a throttle it can be useful to check the box for a "Centered" axis that has a reverse and forward with a middle that is "off". Increase the deadzone to make the middle "off" position larger and easier to find.

### Invert
![Pictgure of Invert Checkbox](images/CH_Manager_invert_checkbox.png)

When this box is checked the axis will be inverted. Use this when the axis is providing the reverse of what you want. For example if a throttle moving forward causes your ship to move backwards you probably want to invert the throttle axis to reverse that. This setting is available in Star Citizen as well but will get reset often due to updates.

### Deadzone
![Picture of Deadzone Field](images/CH_Manager_deadzone_field.png)

A deadzone is the area around the calibrated center position where the axis is considered as being at its center point. The larger the deadzone value the larger the "center" of the axis becomes. For a joystick like the Fighterstick a deadzone of 0-2 is sufficient because the stick naturally returns to center very well. However the mini joystick on the Pro Throttle tends to drift from center and needs a larger deadzone of 2-5. When using a throttle that is "centered" with a forward and back a large deadzone of 10-15 can make it very easy to find "off" by moving the throttle near the middle.

![Animation of No Deadzone](images/CH_Manager_no_dead_zone.gif)
![Animation of No Deadzone](images/CH_Manager_high_dead_zone.gif)

In the animations above the yellow crosshair shows the location of the throttle while the blue dot shows the value sent to Windows. On the left with a deadzone of 0 the blue dot matches the crosshair perfectly, the throttle must be exactly in the center to be considered "off". On the right we have a huge deadzone of 30 so that Windows (blue dot) sees the throttle as in the "center" any time it is within 30% of the calibrated center point.

### Response Curves
![Picture of Response Curve](images/CH_Manager_response_curve.png)
Response curves change how Windows sees input from an axis allowing us to change how sensitive the joystick feels. Choosing the right response curve is highly personal and can drastically change the feeling of a stick, throttle or pedal. Star Citizen supports custom response curves from within the game settings however they will get reset often as part of updates.

CH Control Manager supports 11 response curves that may be selected using the Up/Down arrow buttons. The default is a linear response curve where every change in the stick has an equal corresponding input to Windows. Clicking the Up button will make the axis vertical near the center and horizontal near the ends resulting in a more sensitive center. Clicking the Down button will do the opposite and result in a less sensitive center.

![Animation of High Response in Center](images/CH_Manager_high_response_center.gif)

Clicking Up increases sensitivity near the center and decreases it near the ends. As the throttle moves (yellow crosshair) the input sent to Windows (blue dot) becomes more dramatic near center and "soft" at the ends.

![Animation of Low Response in Center](images/CH_Manager_low_response_center.gif)

Clicking Down decreases sensitivity near the center and increases it near the ends. As the throttle moves (yellow crosshair) the input sent to Windows (blue dot) becomes more "soft" in the center and sensitive near the ends.

## Direct Mode
In Direct Mode the devices appear in Windows with their own names such as `CH Fighterstick USB` and `CH Pro Throttle USB` and operate as they would with the default Windows drivers. This mode does not use the currently loaded mapping, that requires Mapped Mode instead. Remapping to a key or mouse isn't supported in Direct Mode.

### Axis Settings
When using Direct Mode click on the "Test/Calibrate" button, choose a device, then click on the "Axis Settings" tab. Here you can change the response curve, deadzone, etc. used in  Direct Mode.

![Picture of Axis Settings in Direct Mode](images/CHControlManager_Direct_Mode_Mapping.gif)

## Mapped Mode
In Mapped Mode the map is active and the devices it defines will appear in Windows with names such as `CH Control Manager Device 1` and `CH Control Manager Device 2`.

### Axis Settings
For Mapped Mode the axis settings under "DirectX Function" are used and can be saved as part of the map.

![Picture of Axis Settings in Mapped Mode](images/CH_Manager_map_axis_settings.png)

### Map to Key

1. Select the button and uncheck "DX Mode".
2. Right click in the "Press" field under "Normal Action" and choose "Record Keystroke".
3. Press the key or key combination you want the button mapped to.

![Animation of Mapping a Keystroke](images/CH_Manager_map_button_to_key.gif)

### Map to Mouse

1. Select the button or axis.
2. Change the "DirectX Device" under "Normal Action" to "Mouse".
3. Choose the mouse button or axis under "DirectX Control".

## HOTAS Configuration Example

See [this HOTAS guide](../../Configurations/HOTAS/kab_CHProducts/CHControlManagerGuide.md) for a complete example.
