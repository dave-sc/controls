# vJoy
Device driver for a virtual joystick in Windows.

TODO: Link to an official guide for vJoy + Joystick Gremlin that puts together the whole package, this page is only for some vJoy specifics

## How it works
Normally a physical device (joystick, throttle, gamepad) connects to a joystick driver in Windows. The device communicates with the driver which communicates with Windows.

![Normal Joystick to Windows](images/joystick_driver_windows.png)

With a virtual joystick the device is not "real" but appears real to Windows and therefore Star Citizen.

![Joystick to Virtual Joystick](images/virtual_joystick_driver_windows.png)

## Why bother?
When a device communicates with a standard driver we're relying on the manufacturer to provide software to configure a device. Each manufacturer offers varying levels of support via proprietary software that only works with their devices.

Using a virtual joystick with a feeder application such as Joystick Gremlin unlocks configuration options not offered by manufacturers. Using Joystick Gremlin as an example we can create advanced sensitivity curves, merge axes, map multiple devices to the same vJoy button, axis or hat, map buttons to keyboard or mouse input and so on. We can do this for any device, including mixing VPC, Thrustmaster and CH Products devices together and for any game without relying on game options or proprietary software.

## Installation
Instructions provided for Windows 10 for convenience. Install at your own risk. No warranties or guarantees of any kind, express or implied.

### Download
Download `vJoySetup.exe` from the [official source](https://sourceforge.net/projects/vjoystick/files/Beta%202.x/2.1.9.1-160719/) 

### Install
Execute `vJoySetup.exe` to install vJoy. Select `vJoy Configuration application` under `Companion Applications` during installation.

![Installation Components](images/vJoy_install.png)

### Configuration
Using the Windows bar look for an application named `Configure vJoy` and click `Run as Administrator`. Configure vJoy to provide 8 axes, 128 buttons and 4 POV hats. This is more than enough for most configurations however you may add more devices as necessary (up to 16.)

![vJoy Configuration](images/vJoy_Settings.png)

Click `Apply` once and then ***wait patiently as the application may freeze for 30 seconds or more***.

Once finished you may need to restart your computer to see the new device.

### Feeder Application
Use an application like Joystick Gremlin to "feed" input to your virtual device. Feeder applications are beyond the scope of this document.

TODO: another link to the complete vJoy + gremlin guide once ready
