# VPC Constellation Alpha (HOSAS) Keybinds for Star Citizen 3.10
Using an ALPHA-R and ALPHA-L for a dual stick setup. All hats configured as 4-way in the VPC profile for each stick. Brake lever sliders should be merged together as a single axis and mapped to vJoy Y Rotation for analog thrusters without using the twist.

Each stick provides up to 6 axes and 31 buttons for a total of 12 axes and 62 buttons at most. A single vJoy device can support 8 axes and 128 buttons therefore you should only need more than one vJoy device if you intend to use more than 8 separate axis bindings. This largely depends on how you intend to use the brake levers, twist axes and analog mini joysticks.

In this configuration we're using 6 axes for pitch, yaw, roll, throttle, strafe up/down and strafe left/right. The brake levers provide an alternative to the left twist and thus re-use the Z Rotation Axis in vJoy. One of the analog mini joysticks can be mapped to control the mouse cursor which does not require a vJoy axis either. That leaves two remaining unmapped vJoy axis to use with the other mini joystick.

## Modifiers
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Modifier Key 1 (M1)                          | Right Alt              |                      |
| Modifier Key 2 (M2)                          | Right Ctrl             |                      |

## Voice
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Push-to-Talk                                 |                        |                      |               
| Priority Push-to-Talk                        |                        |                      |              
| Mute Sound                                   |                        |                      |               

# Star Citizen

## Flight - Cockpit
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Eject                                        | Y                      | Button 11            |           
| Exit Seat                                    | Y                      | Button 11            |
| Self Destruct                                | Backspace              | Button 12            |
| Emergency Exit Seat                          | Left Shift + U         | Button 13            |
| Increase Cooler Rate                         |                        | Button 14            |
| Decrease Cooler Rate                         |                        | Button 15            |       
| Flight / Systems Ready                       | R                      | Button 16            |
| Open/Close Doors (Toggle)                    |                        | Button 17            |
| Lock/Unlock Doors (Toggle)                   |                        | Button 18            |

## Flight - View
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Look up / down (axis)                        | Y Axis (mouse)         | Y Axis               | Y Rotation Axis (L)
| Look left / right (axis)                     | X Axis (mouse)         | X Axis               | X Rotation Axis (L)
| Cycle camera view                            | F4                     | Button 19            | Button 5 (L)
| Zoom in (3rd person)                         | Mouse Wheel Up         |                      | Wheel Up (L)
| Zoom out (3rd person)                        | Mouse Wheel Down       |                      | Wheel Down (L)
| Freelook (Hold)                              | Z                      | Button 20            |
| Dynamic Zoom In and out (rel.) (axis)        | Left Alt + Mouse Wheel |                      |
| Dynamic Zoom In (rel.)                       |                        | Button 21            |
| Dynamic Zoom Out (rel.)                      |                        | Button 22            |
| Dynamic Zoom In and Out (abs.) (axis)        |                        |                      |
| Dynamic Zoom Toggle (abs.)                   |                        | Button 23            |
| Look behind                                  |                        | Button 24            |

## Flight - Movement
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Pitch (axis)                                 | Y Axis (mouse)         | Y Axis               | Y Axis (R)
| Yaw (axis)                                   | X Axis (mouse)         | X Axis               | X Axis (R)
| Roll (axis)                                  |                        | Z Rotation Axis      | Z Axis (R)
| Throttle forward / back (axis)               |                        | Z Axis               | Y Axis (L)
| Strafe up / down (axis)                      |                        | Y Rotation Axis      | Z Axis (L) or Brake Levers
| Strafe left / right (axis)                   |                        | X Rotation Axis      | X Axis (L)
| E.S.P. (Toggle)                              |                        | Button 25            |
| Speed Limiter (rel.) (axis)                  | Mouse Wheel            |                      |
| Speed Limiter (abs.) (axis)                  |                        |                      |
| Acceleration Limiter Up / Down (rel.) (axis) | Right Alt + Mouse Wheel|                      |
| Acceleration Limiter Up / Down (abs.) (axis) |                        |                      |
| Afterburner                                  | Left Shift             | Button 26            |
| Spacebrake                                   | X                      | Button 27            |
| Speed Limiter (Toggle)                       |                        | Button 28            |
| G-force safety (Toggle)                      |                        | Button 29            |
| VTOL (Toggle)                                | K                      | Button 30            |
| Cruise Control (Toggle)                      | C                      | Button 31            |
| Decoupled mode (Toggle)                      | V                      | Button 32            |
| Quantum Travel System (Toggle)               | B                      | Button 33            |
| Quantum Drive (Hold)                         | B                      | Button 33            |
| Landing System (Toggle)                      | N                      | Button 34            |
| Autoland (Hold)                              | N                      | Button 34            |

## Flight - Targeting
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Pin Index 1 - Lock / Unlock Pinned Target    | 1                      | Button 46            |
| Pin Index 2 - Lock / Unlock Pinned Target    | 2                      | Button 47            |
| Pin Index 3 - Lock / Unlock Pinned Target    | 3                      | Button 48            |
| Pin Index 1 - Pin / Unpin Selected (Hold)    |                        | Button 49            |
| Pin Index 2 - Pin / Unpin Selected (Hold)    |                        | Button 50            |
| Pin Index 3 - Pin / Unpin Selected (Hold)    |                        | Button 51            |
| Lock Selected Target                         |                        | Button 52            |
| Unlock Locked Target                         | Left Alt + T           | Button 53            |
| Pin Selected Target (Hold)                   |                        | Button 54            |
| Unpin Selected Target (Hold)                 |                        | Button 55            |
| Remove All Pinned Targets                    | 0                      | Button 56            |
| Enable / Disable Look Ahead                  | Left Alt + L           | Button 57            |
| Look at Locked Target (Toggle, Hold)         |                        | Button 58            |
| Auto Zoom on Selected Target (Toggle, Hold)  |                        | Button 59            |
| Switch Flight Lead / Lag Reticle (Toggle)    |                        | Button 60            |

## Flight - Target Cycling
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Cycle Selection - Back                       |                        | Button 61            |
| Cycle Selection - Forward                    | R                      | Button 62            |
| Cycle Selection - Reset to Auto              | Left Alt + R           | Button 63            |
| Cycle Lock - In View - Back                  |                        | Button 64            |
| Cycle Lock - In View - Forward               | T                      | Button 65            |
| Cycle Lock - In View - Under Reticle         | T                      | Button 65            |
| Cycle Lock - Pinned - Back                   |                        | Button 66            |
| Cycle Lock - Pinned - Forward                |                        | Button 67            |
| Cycle Lock - Pinned - Reset to First         |                        | Button 68            |
| Cycle Lock - Attackers - Back                |                        | Button 69            |
| Cycle Lock - Attackers - Forward             | 4                      | Button 70            |
| Cycle Lock - Attackers - Reset to Closest    | Left Alt + 4           | Button 71            |
| Cycle Lock - Hostiles - Back                 |                        | Button 72            |
| Cycle Lock - Hostiles - Forward              | 5                      | Button 73            |
| Cycle Lock - Hostiles - Reset to Closest     | Left Alt + 5           | Button 74            |
| Cycle Lock - Friendlies - Back               |                        | Button 75            |
| Cycle Lock - Friendlies - Forward            | 6                      | Button 76            |
| Cycle Lock - Friendlies - Reset to Closest   | Left Alt + 6           | Button 77            |
| Cycle Lock - All - Back                      |                        | Button 78            |
| Cycle Lock - All - Forward                   | 7                      | Button 79            |
| Cycle Lock - All - Reset to Closest          | Left Alt + 7           | Button 80            |
| Cycle Lock - Sub-Target - Back               |                        | Button 81            |
| Cycle Lock - Sub-Target - Forward            | 8                      | Button 82            |
| Cycle Lock - Sub-Target - Reset to Main      | Left Alt + 8           | Button 83            |

## Flight - Target Hailing
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Hail Target                                  | 9                      | Button 39            |

## Flight - Scanning
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Scanning Mode (Toggle)                       | Tab                    | Button 8             | Button 6 (L)
| Activate Scanning                            | Button 1 (mouse)       | Button 1             | Button 1/2 (R)
| Scanning Radar Ping                          | Button 2 (mouse)       | Button 2             | Button 3 (R)
| Scanning Increase Radar Angle                | Mouse Wheel Up         |                      | Wheel Up (L)
| Scanning Decrease Radar Angle                | Mouse Wheel Down       |                      | Wheel Down (L)

## Flight - Mining
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Mining Mode (Toggle)                         | M            *        | Button 9             | Button 12 (L)
| Fire Mining Laser (Toggle)                   | Button 1 (mouse)       | Button 1             | Button 1/2 (R)
| Switch Mining Laser (Toggle)                 | Button 2 (mouse)       | Button 2             | Button 3 (R)
| Increase Mining Laser Power                  | Mouse Wheel Up         |                      | Wheel Up (L)
| Decrease Mining Laser Power                  | Mouse Wheel Down       |                      | Wheel Down (L)
| Activate Mining Consumable 1                 | Left Alt + 1           | Button 35            |
| Activate Mining Consumable 2                 | Left Alt + 2           | Button 36            |
| Activate Mining Consumable 3                 | Left Alt + 3           | Button 37            |
| Jettison Cargo                               | Left Alt + J           | Button 38            |

## Turrets
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Pitch (axis)                                 | Y Axis (mouse)         | Y Axis               | 
| Yaw (axis)                                   | X Axis (mouse)         | X Axis               | 
| E.S.P. (Toggle)                              | E                      |                      |
| Turret Mouse Movement (VJoy/FPS)             | Q                      |                      |
| Recenter Turret (Hold)                       | C                      |                      |
| Turret Gyro Stabilization (Toggle)           | G                      |                      |
| Turret Speed Limiter (Toggle)                | Left Shift             |                      |
| Speed Limiter (rel.) (axis)                  | Mouse Wheel            |                      |
| Velocity Limiter Increase (rel.)             | W                      |                      |
| Velocity Limiter Decrease (rel.)             | S                      |                      |
| Cycle fire mode (staggered / combined)       | V                      |                      |
| Exit Remote Turret                           | Y                      |                      |

## Flight - Weapons
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Fire Weapon Group 1                          | Button 1 (mouse)       | Button 1             | Button 1/2 (R)
| Fire Weapon Group 2                          | Button 2 (mouse)       | Button 2             | Button 3 (R)
| Cycle Weapon Ammo                            |                        | Button 3             | Button 19 (R)
| Gimbal Mode (Toggle, Hold)                   | Left Alt + G           |                      |
| Cycle Gimbal Assist                          | G                      | Button 10            |
| Acquire missile lock (Tap)                   | Button 3 (mouse)       | Button 4             | Button 4/6 (R)
| Release missile lock (Tap)                   |                        | Button 5             | Button 12 (R)
| Launch missile (Hold)                        | Button 3 (mouse)       | Button 4             | Button 4/6 (R)

## Flight - Defensive
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Launch countermeasure                        | H                      | Button 6             | Button 18 (R)
| Cycle countermeasure ammo                    | J                      | Button 7             | Button 20 (R)
| Shield raise level front                     | Numpad 8               | Button 84            |
| Shield raise level back                      | Numpad 2               | Button 85            |
| Shield raise level left                      | Numpad 4               | Button 86            |
| Shield raise level right                     | Numpad 6               | Button 87            |
| Shield raise level top                       | Numpad 7               | Button 88            |
| Shield raise level bottom                    | Numpad 9               | Button 89            |
| Shield reset levels                          | Numpad 5               | Button 90            |

## Flight - Power
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Toggle Power - All                           | U                      | Button 91            |
| Toggle Power - Shields                       | O                      | Button 92            |
| Toggle Power - Weapons                       | P                      | Button 93            |
| Toggle Power - Thrusters                     | I                      | Button 94            |
| Increase Priority - Thrusters                | F5                     | Button 95            |
| Increase Priority - Shields                  | F6                     | Button 96            |
| Increase Priority - Weapons                  | F7                     | Button 97            |
| Reset Priority                               | F8                     | Button 98            |

## Flight - Radar
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| Personal ID Broadcast System (Toggle)        |                        | Button 44            |
| Radar cycle range                            |                        | Button 45            |

## Flight - HUD
| Name                                         | Mouse & Keyboard       | vJoy                 | VPC Device   
|----------------------------------------------|------------------------|----------------------|------------------------|
| mobiGlass (Toggle)                           | F1                     | Button 40            |
| Scoreboard                                   | Tab                    | Button 41            |
| Map                                          | F2                     | Button 42            |
| Wipe Helmet Visor                            | Left Shift + X         | Button 43            |
