# 3.10 HOTAS Setup with CH Control Manager

This guide covers setup for a HOTAS configuration using CH Control Manager which requires using CH Products devices. As a HOTAS guide the assumption is you will be playing with at least the CH Fighterstick and CH Pro Throttle. The CH Pro Pedals are convenient for the additional axis but far more useful in a Joystick Gremlin setup where we have more options for the toe brakes.

## Pricing
Expect to pay about $350 - $400 for a complete set including pedals.

| Product                 | Price        | Notes            
|-------------------------|--------------|-------              
| CH Fighterstick USB     | $120 - $160  | Solid stick, no twist axis, a little big
| CH Pro Throttle USB     | $120 - $160  | Great mini joystick, 3 4-way hats
| CH Pro Pedals USB       | $120 - $160  | Nice for roll/yaw especially in HOTAS, a lot to pay for just 1 to 3 axis

## Quick Start
Follow the **Setup Guide** below for instructions on loading these files.

| Program | File | Notes
|---------|------|-------
| CH Control Manager | [kab_SC_HOTAS.map](files/kab_SC_HOTAS.map) | Map with Pro Pedals bindings
| CH Control Manager | [kab_SC_HOTAS_no_pedals.map](files/kab_SC_HOTAS_no_pedals.map) | Map without Pro Pedals bindings (TODO)
| Star Citizen | [kab_CH_HOTAS.xml](files/kab_HOTAS_CH_3dot10.xml) | Keybinds with Pro Pedals
| Star Citizen | [kab_CH_HOTAS_no_pedals.xml](files/kab_CH_HOTAS_no_pedals.xml) | Keybinds without Pro Pedals (TODO)

See the [CH Control Manager Guide](../../../Software/CHControlManager/) for more information on response curves, dead zones and using the software in direct or mapped mode.

## Keybinds

[Complete keybinds with keyboard and mouse defaults](keybinds.md)

## Printable Bindings

Printable templates with keybinds for these files.

![Fighterstick Mappings](CHFighterStickMappingKab.png)

![Pro Throttle Mappings](CHProThrottleMappingKab.png)

See the [CH Products device guide](../../../Devices/CHProducts/README.md) for blank templates for your own custom bindings.

## Setup Guide

### Installation
Download the control manager from the [CH website](http://www.chproducts.com/13-29620-Downloads-and-Community-Links.php.html) where it may be listed as "optional control manager programming software". Execute the installer and follow the prompts.

### Download the Map
Download [kab_SC_HOTAS.map](files/kab_SC_HOTAS.map) or [kab_SC_HOTAS_no_pedals.map](files/kab_SC_HOTAS_no_pedals.map) if you're not using pedals. The file can be stored anywhere, but you may want to place it under `%DOCUMENTS%/CH Control Manager/Maps` which is the default save location. 

### Load the Mapping
![Animation of Loading a Map](images/CH_Manager_load_map_file.gif)

1. Launch the CH Control Manager software in Windows.
2. Click the "Load" button in CH Control Manager.
3. Navigate to the `kab_SC_HOTAS.map` or `kab_SC_HOTAS_no_pedals.map` file and click "Open".
4. Click "Download" to apply the mapping which puts the software into ***Mapped Mode***.

### Download Keybinds
Download [kab_CH_HOTAS.xml](files/kab_CH_HOTAS.xml) or [kab_CH_HOTAS.xml](files/kab_CH_HOTAS_no_pedals.xml) if you're not using pedals. The file should be copied to `LIVE/USER/Controls/Mappings` under your Star Citizen directory. If you're playing on PTU it will be `PTU/USER/Controls/Mappings` instead.

### Load the Keybinds
![Animation of Loading a Map](images/SC_load_keybinds.gif)

1. Launch Star Citizen with the XML files downloaded and copied to `USER/Controls/Mappings` under your Star Citizen directory.
2. Tap the tilde (~) key to open the console.
3. Type `pp_RebindKeys kab_CH_HOTAS` or `pp_RebindKeys kab_CH_HOTAS_no_pedals` if you're not using pedals.
4. Use Arena Commander to check that the joystick is mapped to pitch/yaw and the throttle to strafing. If they are reversed open the console and type `pp_resortdevices joystick 1 2` to swap the keybinds between the throttle and joystick.

At this point you're ready to play Star Citizen.

## Guide

### Add Devices to Mapping

![Adding devices to a mapping](images/AddSticksToManager.gif)

Add all the devices you want to configure to the mapping. In these examples we're configuring a HOTAS with pedals setup. The order is important. The first device you add by default will show up as CH Control Manager Device 1.

If you intend to use Direct Mode do not load or save a mapping. Click Test/Calibrate and then click on Axis Settings.

### Fighterstick Settings

![Axis settings for Fighterstick](images/FighterstickSettings.gif)

Add a dead zone of 1% or 2% by setting it to "1" or "2". This will prevent the ship from drifting when the joystick is resting very slightly off center.

### Pro Throttle Settings

![Axis settings for Pro Throttle](images/ProThrottleSettings.gif)

Invert the throttle (Z-Axis) as it will be backwards in Star Citizen by default.

If you want "back" on the throttle to move you in reverse check "Center" on the throttle (Z-Axis). If you want full back on the throttle to be "stopped" uncheck "Center" on the throttle (Z-Axis). If center is unchecked you will effectively have no reverse and will need to setup a keybind to change from forward throttle to reverse throttle. This guide does not cover that scenario.

If you chose a "centered" throttle give yourself a large (10 - 15) dead zone so it will be easier for you to stop by moving the throttle to the middle.

Consider changing the response curve by clicking the down arrow 1 to 3 times. Each click will change the throttle to be less sensitive at the low ends to enhance precision at low speeds.

Change the mini joystick (X-Axis & Y-Axis) to have a moderate dead zone (3-5) because the stick tends to rest slightly off center.

If you are using the mini joystick for thruster control and want "up" to move the ship "up" then invert the Y-Axis.

Consider changing the response curves for the mini joystick by clicking the down arrow 1 - 3 times to make them less sensitive near center for control.

### Pro Pedals Settings

![Axis settings for Pro Pedals](images/ProPedalsSettings.gif)

For a HOTAS setup the rudder axis (Z-Axis) is perfect for roll or yaw, whichever you may be missing from your stick and throttle depending on setup. 

In JoyToKey the X-Axis and Y-Axis can be mapped to mouse wheel and used to adjust the speed limiter, mining laser power and anything else bound to the mouse wheel.

TODO: link to a JoyToKey example of the mouse wheel mapping

### Save Mapping & Download

![Saving and downloading a mapping](images/SaveSettings.gif)

Save the mapping so that you have it for future use. Click "Download" to apply the mapping. The software will now be in mapped mode.
