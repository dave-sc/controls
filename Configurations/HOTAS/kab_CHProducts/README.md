# CH Products HOTAS Setup for Star Citizen 3.10

Recommended: setup JoyToKey, vJoy and Joystick Gremlin.

TODO: Link when guide available

Alternatively if you are using CH Products exclusively and want to setup a HOTAS configuration you may [follow this guide](CHControlManagerGuide.md).

# Keybinds

[Complete keybind mapping with keyboard + mouse defaults](keybinds.md)

# Printable Bindings

![Fighterstick Mappings](CHFighterStickMappingKab.png)
![Pro Throttle Mappings](CHProThrottleMappingKab.png)

# Star Citizen XML

[Load mapping into Star Citizen 3.10](kab_CH_HOTAS.xml)
