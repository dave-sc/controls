`# CH Control Manager / JoyToKey / TeamSpeak / etc.

Modifiers are set to a control key such as Alt, Ctrl or Shift. Star Citizen, CH Control Manager and JoyToKey can all specify the left or right Alt/Ctrl/Shift. The left version is more commonly used on the keyboard and in other games, the right can often be devoted to your joystick bindings without interfering with other normal keyboard and mouse bindings. 

Use push-to-talk for Discord, TeamSpeak, Ventrilo, Mumble, etc. with priority push-to-talk as an alternate binding for a command channel or a priority push-to-talk binding in applications like Discord.

## Modifiers
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Modifier Key 1 (M1)                          | Right Alt              | Button 4 (Pro Throttle)
| Modifier Key 2 (M2)                          | Right Ctrl             | Button 3 (Pro Throttle)

## Voice
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Push-to-Talk                                 |                        | Button 4 (Fighterstick)
| Priority Push-to-Talk                        |                        | (M1) Button 4 (Fighterstick)
| Mute Sound                                   |                        | (M2) Button 4 (Fighterstick)

# Game Bindings

## Flight - Cockpit
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Eject                                        | Y                      | (M2) Button 9 (Fighterstick)
| Exit Seat                                    | Y                      | (M2) Button 9 (Fighterstick)
| Self Destruct                                | Backspace              |
| Emergency Exit Seat                          | Left Shift + U         |
| Increase Cooler Rate                         |                        | Button 5 (Pro Throttle)
| Decrease Cooler Rate                         |                        | Button 7 (Pro Throttle)
| Flight / Systems Ready                       | R                      | (M1) Button 4 (Pro Throttle)
| Open/Close Doors (Toggle)                    | K                      | (M1) Button 10 (Fighterstick)
| Lock/Unlock Doors (Toggle)                   | L                      | (M2) Button 10 (Fighterstick)

## Flight - View
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Look left                                    |                        | POV Hat Up (Fighterstick)
| Look right                                   |                        | POV Hat Right (Fighterstick)
| Look left / right                            | X-Axis (mouse)         | X-Axis (Fighterstick)
| Look up                                      |                        | POV Hat Up (Fighterstick)
| Look down                                    |                        | POV Hat Down (Fighterstick)
| Look up / down                               | Y-Axis (mouse)         | Y-Axis (Fighterstick)
| Cycle camera view                            | F4                     | (M1) Button 11 (Fighterstick)
| Cycle camera orbit mode                      |                        |
| Zoom in (3rd person)                         | Mouse Wheel Up         | Right Pedal (Pro Pedals)
| Zoom out (3rd person)                        | Mouse Wheel Down       | Left Pedal (Pro Pedals)
| Freelook (Hold)                              | Z                      | Button 4 (Pro Throttle)
| Dynamic zoom in and out (rel.)               | Left Alt + Mouse Wheel | (M1) Left / Right Pedal (Pro Pedals)
| Dynamic zoom in (rel.)                       |                        |
| Dynamic Zoom Toggle (abs.)                   |                        |
| Look behind                                  |                        | Button 11 (Fighterstick)

## Flight - Movement

### Directional
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch                                        | Y-Axis (mouse)         | Y-Axis (Fighterstick)
| Yaw                                          | X-Axis (mouse)         | X-Axis (Fighterstick)
| Cycle mouse mode                             |                        |
| Roll                                         |                        | Rudder (Pro Pedals)
| Swap Yaw / Roll (Toggle)                     |                        |
| Lock Pitch / Yaw Movement (Toggle, Hold)     | Right Shift            |
| E.S.P. (Toggle)                              |                        | (M1) Button 12 (Fighterstick)

### Limiters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Speed Limiter (rel.)                         | Mouse Wheel            | Left / Right Pedal (Pro Pedals)
| Acceleration Limiter Up / Down (rel.)        | Right Alt + Mouse Wheel| (M1) Left / Right Pedal (Pro Pedals)
| Speed Limiter (Toggle)                       |                        | Button 12 (Fighterstick)
| G-force safety (Toggle)                      |                        | (M2) Button 12 (Fighterstick)

### Engines & Thrusters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Afterburner                                  | Left Shift             | Button 5 (Pro Throttle)
| Spacebrake                                   | X                      | Button 7 (Pro Throttle)
| Throttle forward / back                      |                        | Z-Axis (Pro Throttle)
| Strafe up / down                             |                        | Y-Axis (Pro Throttle)
| Strafe left / right                          |                        | X-Axis (Pro Throttle)
| Decoupled mode (Toggle)                      | V                      | Button 8 (Pro Throttle)
| VTOL (Toggle)                                | J                      | (M1) Button 8 (Pro Throttle)
| Cruise Control (Toggle)                      | C                      | (M2) Button 8 (Pro Throttle)

### Quantum & Landing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Quantum Travel System (Toggle)               | B                      | Button 14 (Pro Throttle)
| Quantum Drive (Hold)                         | B                      | Button 14 (Pro Throttle)
| Landing System (Toggle)                      | N                      | Button 13 (Pro Throttle)
| Autoland (Hold)                              | N                      | Button 13 (Pro Throttle)

## Flight - Targeting
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock / Unlock Selected Target                | 1                      | Button 5 (Fighterstick)
| Lock Nearest Attacker                        | 2                      | Button 7 (Fighterstick)
| Lock Next Pinned Target                      | 4                      | Button 6 (Fighterstick)
| Lock Previous Pinned Target                  | 5                      | Button 8 (Fighterstick)
| Unlock Locked Target                         |                        | (M1) Button 7 (Fighterstick)

### Pinning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pin / Unpin Selected Target                  | 3                      | (M1) Button 5 (Fighterstick)
| Pin Index 1 - Lock / Unlock Pinned Target    | Left Alt + 1           | (M1) Button 16 (Pro Throttle)
| Pin Index 1 - Pin / Unpin Selected Target    | Left Alt + 1           | (M2) Button 16 (Pro Throttle)
| Pin Index 2 - Lock / Unlock Pinned Target    | Left Alt + 2           | (M1) Button 13 (Pro Throttle)
| Pin Index 2 - Pin / Unpin Selected Target    | Left Alt + 2           | (M2) Button 13 (Pro Throttle)
| Pin Index 3 - Lock / Unlock Pinned Target    | Left Alt + 3           | (M1) Button 14 (Pro Throttle)
| Pin Index 3 - Pin / Unpin Selected Target    | Left Alt + 3           | (M2) Button 14 (Pro Throttle)
| Remove All Pinned Targets                    | 0                      | (M1) Button 15 (Pro Throttle)

### Selection
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hostiles - Select Next Target in View        |                        | (M1) Button 6 (Fighterstick)
| Hostiles - Select Previous Target in View    |                        | (M1) Button 8 (Fighterstick)
| Friendlies - Select Next Target in View      |                        | (M2) Button 6 (Fighterstick)
| Friendlies - Select Previous Target in View  |                        | (M2) Button 8 (Fighterstick)
| All Targets - Select Next Target in View     |                        | (M2) Button 5 (Fighterstick)
| All Targets - Select Previous Target in View |                        | (M2) Button 7 (Fighterstick)
| Reset Selection                              |                        | Button 7 (Fighterstick)

### Sub-Target
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock Next Sub-Target                         |                        | (M2) Button 5 (Fighterstick)
| Lock Previous Sub-Target                     |                        | (M2) Button 7 (Fighterstick)
| Reset Sub-Target                             |                        | (M2) Button 7 (Fighterstick) (if hold works)

### Utility
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Look at Locked Target (Toggle, Hold)         |                        | Button 16 (Fighterstick)
| Auto Zoom on Selected Target (Toggle, Hold)  |                        | (M1) Button 16 (Fighterstick)
| Look Ahead (Toggle)                          | Left Alt + R           | (M2) Button 16 (Fighterstick)

### Scanning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Scanning Mode (Toggle)                       | Tab                    | (M2) Button 3 (Fighterstick)
| Activate Scanning                            | Button 1 (mouse)       | Button 1 (Fighterstick)
| Scanning Radar Ping                          | Button 2 (mouse)       | Button 2 (Fighterstick)
| Scanning Increase Radar Angle                | Mouse Wheel Up         | Right Pedal (Pro Pedals)
| Scanning Decrease Radar Angle                | Mouse Wheel Down       | Left Pedal (Pro Pedals)

## Flight - Weapons
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Fire Weapon Group 1                          | Button 1 (mouse)       | Button 1 (Fighterstick)
| Fire Weapon Group 2                          | Button 2 (mouse)       | Button 3 (Fighterstick)
| Cycle Weapon Ammo                            |                        |
| Cycle Weapon Ammo (Back)                     |                        |
| Cycle Gimbal Assist                          | R                      | Button 14 (Fighterstick)
| Switch Flight Lead / Lag Reticle (Toggle)    |                        | (M1) Button 14 (Fighterstick)
| Gimbal Mode (Toggle, Hold)                   |                        | (M2) Button 14 (Fighterstick)
| Acquire missile lock (Tap)                   | Button 3 (mouse)       | Button 2 (Fighterstick)
| Release missile lock (Tap)                   |                        | Button 2 (Fighterstick)
| Launch missile (Hold)                        | Button 3 (mouse)       | Button 2 (Fighterstick)

## Flight - Defensive
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Launch countermeasure                        | G                      | Button 13 (Fighterstick)
| Cycle countermeasure ammo                    | H                      | (M1) Button 13 (Fighterstick)
| Cycle countermeasure ammo (Back)             |                        | (M2) Button 13 (Fighterstick)
| Shield raise level front                     | Numpad 8               | POV Hat Up (Pro Throttle)
| Shield raise level back                      | Numpad 2               | POV Hat Down (Pro Throttle)
| Shield raise level left                      | Numpad 4               | POV Hat Left (Pro Throttle)
| Shield raise level right                     | Numpad 6               | POV Hat Right (ProThottle)
| Shield raise level top                       | Numpad 7               | (M1) POV Hat Up (Pro Throttle)
| Shield raise level bottom                    | Numpad 9               | (M1) POV Hat Down (Pro Throttle)
| Shield reset levels                          | Numpad 5               | (M1) POV Hat Left (Pro Throttle)

## Turrets
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch                                        | Y-Axis (mouse)         | Y-Axis (Fighterstick)
| Yaw                                          | X-Axis (mouse)         | X-Axis (Fighterstick)
| Turret Speed Limiter (Toggle)                | Left Shift             | Button 12 (Fighterstick)
| E.S.P. (Toggle)                              | E                      | (M1) Button 12 (Fighterstick)
| Turret Gyro Stabilization (Toggle)           | G                      | (M2) Button 12 (Fighterstick)
| Turret Mouse Movement (Toggle)               | Q                      |
| Recenter Turret (Hold)                       | C                      |
| Speed Limiter (rel.)                         | Mouse Wheel            | Left / Right Pedal (Pro Pedals)
| Velocity Limiter Increase (rel.)             | W                      |
| Velocity Limiter Decrease (rel.)             | S                      |
| Cycle fire mode (staggered / combined)       | V                      |
| Exit Remote Turret                           | Y                      |

## Flight - Power
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Increase Priority - Shields                  |                        | Button 12 (Fighterstick)
| Increase Priority - Thrusters                |                        | Button 9 (Fighterstick)
| Increase Priority - Weapons                  |                        | Button 10 (Fighterstick)
| Reset Priority                               |                        | Button 11 (Fighterstick)
| Toggle Power - Shields                       |                        | (M1) Button 12 (Fighterstick)
| Toggle Power - Thrusters                     |                        | (M1) Button 9 (Fighterstick)
| Toggle Power - Weapons                       |                        | (M1) Button 10 (Fighterstick)
| Toggle Power - All                           | U                      | (M1) Button 11 (Fighterstick)
| Increase Throttle                            |                        |
| Increase Throttle to Max                     |                        |
| Decrease Throttle                            |                        |
| Decrease Throttle to Min                     |                        |

## Flight - Radar
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Personal ID Broadcast System (Toggle)        |                        | Button 15 (Pro Throttle)
| Radar cycle range                            |                        | Button 16 (Pro Throttle)

## Flight - HUD
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Map                                          | F2                     | Button 9 (Fighterstick)
| mobiGlass (Toggle)                           | F1                     | (M1) Button 9 (Fighterstick)
| Wipe Helmet Visor                            | Left Shift + Z         | (M2) Button 9 (Fighterstick)

## Flight - Mining
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Mining Mode (Toggle)                         |                        | (M2) Button 3 (Fighterstick)
| Fire Mining Laser (Toggle)                   |                        | Button 1 (Fighterstick)
| Switch Mining Laser (Toggle)                 |                        | Button 2 (Fighterstick)
| Increase Mining Laser Power                  | Mouse Wheel Up         | Right Pedal (Pro Pedals)
| Decrease Mining Laser Power                  | Mouse Wheel Down       | Left Pedal (Pro Pedals)
| Activate Mining Consumable 1                 | Left Alt + 1           | Button 6 (Pro Throttle)
| Activate Mining Consumable 2                 | Left Alt + 2           | (M1) Button 6 (Pro Throttle)
| Activate Mining Consumable 3                 | Left Alt + 3           | (M2) Button 6 (Pro Throttle)
| Jettison Cargo                               | Left Alt + T           | (M2) Button 8 (Pro Throttle)

## Flight - Target Hailing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hail Target                                  | 6                      | (M1) Button 4 (Pro Throttle)
