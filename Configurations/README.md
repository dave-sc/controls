# Unified vJoy Star Citizen Configurations
A configuration is a setup for one or more physical devices (joystick, controller, throttle, pedals, etc) using the unified vJoy keybinds. Each configuration should include an XML mapping for Joystick Gremlin or a detailed guide on how to setup the configuration with the unified vJoy keybinds. 

* HOMAS (Hands on Mouse and Stick)
* HOTAS (Hands on Throttle and Stick)
  * kab's CH Products (Fighterstick + Pro Throttle) 
* HOSAS (Hands on Stick and Stick)
  * kab's VPC Alphas (dual Constellation Alphas)
* HOTSAS (Hands on Throttle/Stick and Stick)
  * kab's CH Products + T-16000M (Fighterstick + Pro Throttle + )
  * kab's VPC Alphas + CH (dual Constellation Alphas + CH Pro Throttle)
