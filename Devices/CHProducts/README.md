# CH Products

Sturdy and reliable devices from an industrial joystick manufacturer. They are suitable for desktop use or with a Monstertech desk mount. What they lack in style they make up for in reliability and usability.

#### Pros

* Fantastic analog mini joystick on the Pro Throttle
* 3x 4-way hat and 8-way POV hat on both Fighterstick and Pro Throttle
* Very durable
* Suitable for desktop or desk mount with Monstertech table mounts
* CH Control Manager can be used in place of JoyToKey/Joystick Gremlin

#### Cons

* No twist axis on the Fighterstick
* No left-handed grip for Fighterstick
* Fighterstick may be too large for small hands
* Lacks the scroll wheel found on higher end products
* JoyToKey/Joystick Gremlin are arguably better than CH Control Manager

## Pricing

| Product                 | Price        | Notes            
|-------------------------|--------------|-------              
| CH Fighterstick USB     | $120 - $160  | Solid stick, no twist axis, a little big.
| CH Pro Throttle USB     | $120 - $160  | Great mini joystick, 3 4-way hats
| CH Pro Pedals USB       | $120 - $160  | Nice for roll/yaw especially in HOTAS, a lot to pay for just 1 to 3 axis
| CH Flightstick Pro USB  | $80 - $90    | Ambidextrous, a more durable but less ergonomic option for the left stick compared to the T-16000M
| Monstertech Table Mount | $99 each     | Bulky, a little heavy, very sturdy, not as sleek as VIRPIL/VKB mounts, devices can move around on the flat mount plate

Expect to spend at least $250 for the stick and throttle for a HOTAS setup. Prices will vary, currently Amazon has both at $120 each with free shipping. The Pro Pedals are usually a similar price. Monstertech builds very sturdy desk mounts with adjustable height.

## How to Use

In terms of budget CH Products provide a number of options because the Fighterstick is much better than the T-16000M while the Pro Throttle is good enough to add to a dual VIRPIL or VKB setup.

### Fighterstick

Use with a Pro Throttle (HOTAS) or with any left-handed stick (HOSAS):

* Pro Throttle for a competitive HOTAS setup ($250+)
* Pro Throttle *and* a left-handed stick for a HOSAS + HOTAS setup ($300+) 
* T-16000M with a left-handed grip for an upgrade over dual T-16000Ms ($180+)
* VIRPIL/VKB with a left-handed grip for an option cheaper than dual VIRPIL/VKB ($450+)

### Pro Throttle

The Pro Throttle gets special mention because of the fantastic analog mini joystick. This effectively allows you to have a joystick at your thumb (great for thrusters!) while you can maintain independent control of the throttle.

As a result this device is very versatile. It can be paired with any right-handed joystick including the Fighterstick for a viable HOTAS setup. It can be added to any existing HOSAS setup, but you will need vJoy to map functions to both the throttle and left stick. This will allow you to switch between HOTAS and HOSAS at any time.  

## Blank Bindings Templates

### Fighterstick
![CH Fighterstick Binding Template](images/CHFighterstickBlank.png)

### Pro Throttle
![CH Pro Throttle Binding Template](images/CHProThrottleBlank.png)

### Pro Pedals
Not available.

### XCF for GIMP

Convenient `.xcf` files for the ***free*** [GIMP image editor](https://www.gimp.org/) to enter your custom mappings before printing.

* CH Fighterstick: [CHFighterStickMappingBlank.xcf](CHFighterStickMappingBlank.xcf)
* CH Pro Throttle: [CHProThrottleMappingBlank.xcf](CHProThrottleMappingBlank.xcf)

## Configurations

TODO: link to recommended JoyToKey + Joystick Gremlin setup

If you're using all CH Products devices you can use the [CH Control Manager software](../../Software/CHControlManager/README.md) instead of Joystick Gremlin. You will likely still want JoyToKey to set device order.

## HOTAS Guides
| Devices | Author | Tools
|---------|--------|------
| [Fighterstick + Pro Throttle + Pro Pedals (Recommended)](../../Configurations/HOTAS/kab_CHProducts/README.md) | kab | Joystick Gremlin
| [Fighterstick + Pro Throttle + Pro Pedals (w/ CH Control Manager)](../../Configurations/HOTAS/kab_CHProducts/CHControlManagerGuide.md) | kab | CH Control Manager

## HOSAS Guides
| Devices | Author | Tools
|---------|--------|------
| None yet

## HOMAS Guides
| Devices | Author | Tools
|---------|--------|------
| None yet

## HOSAS + HOTAS Guides
| Devices | Author | Tools
|---------|--------|------
| None yet
