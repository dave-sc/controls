# Devices
Guides and information for specific devices belong here. Devices should be grouped under a subdirectory for each manufacturer (CH Products, VIRPIL, Thrustmaster, etc). If a manufacturer has more than a few devices then further break up the devices into subdirectories under the manufacturer.

* CH Products
  * Fighterstick
  * Pro Throttle
  * Pro Pedals
* Logitech Saitek
* Thrustmaster
* VIRPIL
  * Constellation Alpha
* VKB
