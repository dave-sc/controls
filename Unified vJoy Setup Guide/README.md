# Star Citizen Unified vJoy Setup
A step-by-step guide on setting up vJoy, Joystick Gremlin and Star Citizen for use with the Unified vJoy Bindings.

TODO: include link to video series

## USE AT YOUR OWN RISK
All information contained herein provided "as is" with no guarantees or warranties, express or implied. Authors and contributors are not responsible for any errors or omissions, or for the results obtained from the use of this information. 

## Why bother?

* Provides advanced options not available in game options or manufacturer software
  * Configure complex sensitivity curves and dead zones per physical device or per vJoy axis
  * Map device buttons or POV hats to keyboard keys, mouse buttons/wheel, profile switching or a complex macro
  * Assign complex bindings that change when you double tap vs single tap, or tap vs hold a button
  * Merge axes so Star Citizen sees a single axis that combines two other axes such as using toe brakes for `Strafe up / down`
* Configure axes, change button assignments, add or remove devices, all without restarting Star Citizen or even opening a menu in game
* Expose a single virtual joystick to Star Citizen so that you never need to use `pp_ResortDevices` again
* Works with almost any Windows compatible device including joysticks, XBox/PS controllers, throttles, pedals, keyboards and more
* Works in almost any Windows compatible game

## How?
Normally Star Citizen uses a physical device directly via Windows:

![Normal Joystick to Windows](images/joystick_driver_windows.png)

Our setup creates a virtual joystick for Star Citizen to use instead:

![Unified vJoy for Star Citizen](images/unified_vjoy_diagram.png)

Now instead of mapping a device to Star Citizen you will map your device to vJoy. This in turn requires that vJoy get mapped to Star Citizen which results in an "extra" step. We're going to remove that extra step by loading `Unified vJoy Std` keybindings in Star Citizen so that we only have to worry about mapping devices in Joystick Gremlin.

## Installation
TL;DR

1. Install vJoy device driver
2. Configure a vJoy device with 128 buttons and 8 axes
3. Install Joystick Gremlin
4. Load `Unified vJoy Std` bindings in Star Citizen
5. Map devices to vJoy in Joystick Gremlin

Continue reading for step-by-step instructions.

### vJoy
Download `vJoySetup.exe` from the [official source](https://sourceforge.net/projects/vjoystick/files/Beta%202.x/2.1.9.1-160719/). Execute `vJoySetup.exe` to install vJoy. 

Select `vJoy Configuration application` under `Companion Applications` during installation.

![Installation Components](images/vJoy_install.png)

Using the Windows bar look for an application named `Configure vJoy` and click `Run as Administrator`. Configure vJoy to provide 8 axes and 128 buttons. POV hats are optional, you will be able to map each direction on your hat to different buttons.

![vJoy Configuration](images/vJoy_Settings.png)

Click `Apply` once and then ***wait patiently as the application may freeze for 30 seconds or more***. Once finished you may need to restart your computer to see the new device.

### Joystick Gremlin
Grab the latest from the [official source](https://whitemagic.github.io/JoystickGremlin/download/). Execute the Joystick Gremlin installer and follow the prompts.

### Star Citizen
Copy `layout_Unified_vJoy_Std.xml` to your `<Star Citizen Home>/USER/Controls/Mappings/` directory.

Open Star Citizen and load the `Unified vJoy Std` bindings:

![Load in Star Citizen](images/star_citizen_bindings.gif)

With vJoy installed and configured, Joystick Gremlin installed and Star Citizen configured with our unified keybindings we're ready to configure our devices.

## Configuration
Open Joystick Gremlin **as administrator** (administrator required for HIDGuardian integration):

![Open Joystick Gremlin as Administrator](images/gremlin_open_admin.png)

